<?php
require_once('dao_consulta_sql.php');

class cn_consultas_sql extends toba_cn
{	
	//---------------------------------------------------------------------------------------
	//---- GENERALES ------------------------------------------------------------------
	//-----------------------------------------------------------------------------
	function guardar()
	{
		$this->dep('dr_consultas')->sincronizar();
		$this->dep('dr_consultas')->resetear();
	}
		
	function resetear()
	{
		$this->dep('dr_consultas')->resetear();
	}		
		
	//---------------------------------------------------------------------------------------
	//---- ABM Consulta SQL -----------------------------------------------------
	//-----------------------------------------------------------------------------	
	function agregar_consulta_sql($datos)
	{	
		$id = $this->dep('dr_consultas')->tabla('dt_consulta_sql')->nueva_fila($datos);
		$this->dep('dr_consultas')->tabla('dt_consulta_sql')->set_cursor($id);
		$this->dep('dr_consultas')->sincronizar();
		$this->dep('dr_consultas')->resetear();
	}
    
	function get_consulta_sql()
	{
		if($this->dep('dr_consultas')->tabla('dt_consulta_sql')->esta_cargada()) {			   
			return $this->dep('dr_consultas')->tabla('dt_consulta_sql')->get();			 
		}
	}    
    
    function hay_cursor_consulta_sql()
	{
		return $this->dep('dr_consultas')->tabla('dt_consulta_sql')->hay_cursor();	
	}

	function traer_consulta_sql($seleccion)
	{
		return $this->dep('dr_consultas')->tabla('dt_consulta_sql')->get_id_fila_condicion($seleccion);
	}
	
	function traer_id_memoria_consulta_sql($id)
	{
		if ($this->dep('dr_consultas')->tabla('dt_consulta_sql')->esta_cargada()) {
			return $this->dep('dr_consultas')->tabla('dt_consulta_sql')->get_id_fila_condicion($id);
		}
	}	
	
	function set_consulta_sql($datos)
	{
		$this->dep('dr_consultas')->tabla('dt_consulta_sql')->set($datos);
	}
	
	function set_cursor_consulta_sql($seleccion)
	{
		if ($this->dep('dr_consultas')->tabla('dt_consulta_sql')->esta_cargada()) 
		{
			 $this->dep('dr_consultas')->tabla('dt_consulta_sql')->set_cursor($seleccion);
		}
	}
	
	function cargar_consulta_sql($seleccion=null)
	{
		if (!$this->dep('dr_consultas')->tabla('dt_consulta_sql')->esta_cargada()) {
			if (isset($seleccion)) {	
				$this->dep('dr_consultas')->tabla('dt_consulta_sql')->cargar($seleccion);
		} else {
			$this->dep('dr_consultas')->tabla('dt_consulta_sql')->cargar();
			}
		}
	}
	
	function eliminar_consulta_sql($id_memoria)
	{
		if ($this->dep('dr_consultas')->tabla('dt_consulta_sql')->esta_cargada()) {
			$this->dep('dr_consultas')->tabla('dt_consulta_sql')->eliminar_fila($id_memoria);
		}	
	}
}
?>