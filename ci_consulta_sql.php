<?php
require_once('queries_sql_leu_excepcion.php');
require_once('dao_consulta_sql.php');
class ci_consulta_sql extends toba_ci
{
	protected $s__datos;
	protected $s__datos_formulario;
	protected $s__datos_filtro;
	protected $s__where;
	protected $s__seleccion;
	protected $s__otra_var;
	
	function get_cn()
	{
		return $this->controlador->cn();
	}
	
	//---------------------------------------------------------------------------------
    //---- Eventos del CI -------------------------------------------------------------
	//---------------------------------------------------------------------------------	
	function evt__nuevo()
	{
		$this->set_pantalla('pant_edicion');		
	}
	function evt__volver()
	{
		toba::memoria()->eliminar_dato_instancia('datos');
		$this->set_pantalla('pant_inicial');		
	}
	
	//-----------------------------------------------------------------------------------
	//---- cuadro -----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	function conf__cuadro_consultas(toba_ei_cuadro $cuadro)
	{
		$this->cn()->resetear();
		if (isset($this->s__datos_filtro)) {
			$datos = dao_consulta_sql::get_consultas($this->s__where);
			if($datos){
				$cuadro->set_datos($datos);
			}
		}else{
			$datos = dao_consulta_sql::get_consultas();
			if($datos){
				$cuadro->set_datos($datos);
			}
		}	 
	}
	
	function evt__cuadro_consultas__seleccion($seleccion)
	{	
		if ($this->cn()->hay_cursor_consulta_sql())
		{
				$this->cn()->resetear();
		}
		$this->cn()->cargar_consulta_sql($seleccion);
		$this->s__seleccion = $seleccion;
		$id_memoria = $this->cn()->traer_consulta_sql($seleccion);
		$this->cn()->set_cursor_consulta_sql($id_memoria[0]);	
		$this->set_pantalla('pant_edicion');		
	}
	
	function evt__cuadro_consultas__eliminar($seleccion)
	{
		$this->cn()->cargar_consulta_sql($seleccion);
		$id_memoria = $this->cn()->traer_id_memoria_consulta_sql($seleccion);
		$this->cn()->eliminar_consulta_sql($id_memoria[0]);
		try{
			$this->cn()->guardar();
		}catch (toba_error_db $error) {
			$codigo = $error->get_sqlstate();
			leu_excepcion::get_mensaje_error($codigo, 'Consulta', 'La ');
			$this->cn()->resetear();
		}		
	}
	
	//---------------------------------------------------------------------------------
    //---- Filtro ---------------------------------------------------------------------
	//--------------------------------------------------------------------------------- 		
	function conf__filtro_consultas_sql(toba_ei_filtro $filtro)
	{
		if (isset($this->s__datos_filtro)) {			
			$filtro->set_datos($this->s__datos_filtro);
			$this->s__where = $filtro->get_sql_where();
		}
	}
    
	function evt__filtro_consultas_sql__filtrar($datos)
	{
		$this->s__datos_filtro = $datos;
	}
    
	function evt__filtro_consultas_sql__cancelar()
	{
		unset($this->s__datos_filtro);
	}
	
	//-----------------------------------------------------------------------------------
	//---- form --------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------	
	function conf__form_formulario_consulta(toba_ei_formulario $form)
	{
		if ($this->cn()->hay_cursor_consulta_sql()){
			toba::memoria()->set_dato_instancia('cursor','OK');
		}
		if($this->s__datos_formulario){ 
			$form->set_datos($this->s__datos_formulario);
			unset($this->s__datos_formulario);
		}elseif ($this->cn()->hay_cursor_consulta_sql())
		{		
	        $datos = $this->get_cn()->get_consulta_sql();
			$form->set_datos($datos);
		}
	}
	
	function evt__form_formulario_consulta__modificacion($datos)
	{
		$resultado_validacion = $this->limpiar_cadena_sql($datos['consulta_sql']);
		if ($resultado_validacion == null){		
			if ($this->cn()->hay_cursor_consulta_sql()){			
				$this->cn()->set_consulta_sql($datos);        
				try{
					$this->cn()->guardar();
					toba::notificacion()->agregar(' Los datos de la Consulta se guardaron correctamente.', 'info');
				}catch (toba_error_db $error) {
					$sql_state = $error->get_sqlstate();
					$codigo = $error->get_sqlstate();
					queries_sql_leu_excepcion::get_mensaje_error($codigo, ' Consulta ', 'La ');
					$this->cn()->resetear();
				}
				toba::memoria()->eliminar_dato_instancia('datos');
				$this->set_pantalla('pant_inicial');
			}
		} elseif ($resultado_validacion <> null) {
			toba::notificacion()->agregar(' La consulta contiene comandos NO permitidos', 'error');
			toba::notificacion()->agregar($resultado_validacion);
		}
	}
	
	function evt__form_formulario_consulta__alta($datos)
	{
		$resultado_validacion = $this->limpiar_cadena_sql($datos['consulta_sql']);
		if ($resultado_validacion == null){	
			$this->cn()->resetear();
			$this->cn()->agregar_consulta_sql($datos);
			toba::notificacion()->agregar('Los datos de la Consulta se guardaron correctamente.','info');
			toba::memoria()->eliminar_dato_instancia('datos');
			$this->set_pantalla('pant_inicial');
		} elseif ($resultado_validacion <> null) {
			toba::notificacion()->agregar(' La consulta contiene comandos NO permitidos', 'error');
			toba::notificacion()->agregar($resultado_validacion);
		}	
	}
	
	
	//-----------------------------------------------------------------------------------
	//---- cuadro -----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	function conf__cuadro_resultado_consulta(toba_ei_cuadro $cuadro)
	{			
        if (!empty($this->s__datos)) {
            $datos = $this->s__datos;
            foreach ($datos[0] as $k => $v) {
                $columnas[] = array('clave' => $k, 'titulo' => $k);
            }
            $cuadro->limpiar_columnas();
            $cuadro->agregar_columnas($columnas);
            $cuadro->set_datos($this->s__datos);
			unset($this->s__datos);
        }   
	}
	//-----------------------------------------------------------------------------------
	//---- cuadro_consultas -------------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function evt__cuadro_consultas__ejecutar_sql($seleccion)
	{
		$this->cn()->cargar_consulta_sql($seleccion);
		$this->s__seleccion = $seleccion;
		$id_memoria = $this->cn()->traer_consulta_sql($seleccion);
		$this->cn()->set_cursor_consulta_sql($id_memoria[0]);
		$this->set_pantalla('pant_ejecucion');	
	}

	//-----------------------------------------------------------------------------------
	//---- form_lectura_consulta --------------------------------------------------------
	//-----------------------------------------------------------------------------------

	function conf__form_lectura_consulta(queries_sql_leu_ei_formulario $form)
	{
		if($this->s__datos_formulario){ 
			$form->set_datos($this->s__datos_formulario);
			unset($this->s__datos_formulario);
		}elseif ($this->cn()->hay_cursor_consulta_sql())
		{		
	        $datos = $this->get_cn()->get_consulta_sql();
			$form->set_datos($datos);
		}
	}

	
	
	function limpiar_cadena_sql ($cadena_a_guardar) {
		 $comandos_no_permitidos = array('ALTER', 'BEGIN', 'CREATE', 'REPLACE', 'COPY', 'COMMIT', 'DELETE', 'DECLARE',
		 'DELETE', 'DROP', 'EXECUTE', 'EXPLAIN', 'GRANT', 'INSERT', 'INSERT INTO', 'INTO', 'INSERT',
		 'IMPORT', 'LOCK', 'PERFORM', 'REVOKE', 'RETURN', 'RAISE', 'ROLLBACK', 'SELECT INTO', 'SET',
		 'TABLE', 'UPDATE');
		$cadena_limpia = str_ireplace($comandos_no_permitidos, "", $cadena_a_guardar);
		$resultado = strcasecmp($cadena_a_guardar, $cadena_limpia);
		if ($resultado <> 0) {
			return implode(", ", $comandos_no_permitidos); //error
		}else{
			return null; 
		}
	}
	
	
	
	function evt__form_lectura_consulta__ejecutar($datos)
	{
		$this->s__datos_formulario = $datos;
		$palabra = 'SIN_COINCIDENCIA';
		$keys[1]= 'ALTER';
		$keys[2]= 'BEGIN';
		$keys[3]= 'CREATE';
		$keys[4]= 'REPLACE';
		$keys[5]= 'COPY';
		$keys[6]= 'COMMIT';
		$keys[7]= 'DELETE';
		$keys[8]= 'DECLARE';
		$keys[9]= 'DELETE';
		$keys[10]= 'DROP';
		$keys[11]= 'EXECUTE';
		$keys[12]= 'EXPLAIN';
		$keys[13]= 'GRANT';
		$keys[14]= 'INSERT';
		$keys[15]= 'INSERT INTO';
		$keys[16]= 'INTO';
		$keys[17]= 'INSERT';
		$keys[18]= 'IMPORT';
		$keys[19]= 'LOCK';
		$keys[20]= 'PERFORM';
		$keys[21]= 'REVOKE';
		$keys[22]= 'RETURN';
		$keys[23]= 'RAISE';
		$keys[24]= 'ROLLBACK';
		$keys[25]= 'SELECT INTO';
		$keys[26]= 'SET';
		$keys[27]= 'TABLE';
		$keys[28]= 'UPDATE';
		foreach ($keys as $key){
			$regex = '/[^\s,.]*'.preg_quote($key).'[^\s,.]*/i';			
			if (preg_match_all($regex, $datos['consulta_sql'], $matches)) {
				toba::notificacion()->agregar('Solo se permiten consultas SELECT','error');
				$palabra = 'CON_COINCIDENCIA';
			}			
		}
		if (!empty($datos['consulta_sql'])&& $palabra == 'SIN_COINCIDENCIA') {
			$datos = dao_consulta_sql::ejecutar_consulta($datos['consulta_sql']);
			$campos_quitados='';
			foreach($datos AS $clave1=>$dato1){
				foreach($dato1 AS $clave=>$dato){
					if(gettype($dato) == 'resource'){
						if(strpos($campos_quitados, $clave) === FALSE){
							if($campos_quitados == ''){
								$campos_quitados.= $clave;
							}else{
								$campos_quitados.= ', '.$clave;
							}
						}
						unset($datos[$clave1][$clave]);
					}
				}
			}
			if($campos_quitados!=''){
						toba::notificacion()->agregar('Se eliminaron los campos de tipo Bytea: '.$campos_quitados,'info');
			}
			$this->s__datos = $datos;
			toba::memoria()->set_dato_instancia('datos',$datos);
			$usuario = toba::usuario()->get_id();
			$datos['usuario_ultima_ejecucion'] = $usuario;
			$datos['fecha_ultima_ejecucion'] = date('Y-m-d H:i');
			if ($this->cn()->hay_cursor_consulta_sql())
			{	
				$this->cn()->set_consulta_sql($datos);
				$this->cn()->guardar();
			}
		}
		if($this->s__seleccion){
			$this->cn()->cargar_consulta_sql($this->s__seleccion);
			$id_memoria = $this->cn()->traer_consulta_sql($this->s__seleccion);
			$this->cn()->set_cursor_consulta_sql($id_memoria[0]);
			unset($this->s__seleccion);
		}
	}

	//-----------------------------------------------------------------------------------
	//---- form_formulario_consulta -----------------------------------------------------
	//-----------------------------------------------------------------------------------

	function evt__form_formulario_consulta__cancelar()
	{
		$this->cn()->resetear();
		$this->set_pantalla('pant_inicial');
	}

	function evt__form_lectura_consulta__volver()
	{
		toba::memoria()->eliminar_dato_instancia('datos');
		$this->set_pantalla('pant_inicial');
	}

}
?>