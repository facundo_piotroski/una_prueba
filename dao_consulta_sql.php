<?php
class dao_consulta_sql
{
	static function get_consultas($where =  null)
	{
		if(isset($where)){	
			$sql="SELECT						
						idconsulta_sql
						,nombre  
						,descripcion
						,consulta_sql
						,usuario_ultima_ejecucion
						,fecha_ultima_ejecucion
				FROM 
						consultas_sql.consultas_sql
				WHERE 
						".$where."						
				ORDER BY nombre ASC
						;";
		}else{
			$sql="SELECT						
						idconsulta_sql
						,nombre  
						,descripcion
						,consulta_sql
						,usuario_ultima_ejecucion
						,fecha_ultima_ejecucion						
				FROM 
						consultas_sql.consultas_sql					
				ORDER BY nombre ASC			
						;";						
		}
		return consultar_fuente($sql);		   
	}

	static function ejecutar_consulta($consulta_sql = null)
	{
  		return consultar_fuente($consulta_sql);
	}
	
	static function get_esquemas()
	{
		$sql = "SELECT
					DISTINCT(table_schema) as table_schema
				FROM
					information_schema.tables
				WHERE
					table_schema <> 'pg_catalog'
				ORDER BY table_schema;";
		return consultar_fuente($sql);
	}
	
	static function get_tablas_x_schema($table_schema)
	{
		$sql = "SELECT
					table_schema
					,table_name
				FROM
					information_schema.tables
				WHERE
					table_schema = '$table_schema'				
				ORDER BY table_name;";
		return consultar_fuente($sql);
	}
	
	static function get_columnas_x_tabla($table_name)
	{
		$sql = "SELECT
					column_name
				FROM
					information_schema.columns
				WHERE
					table_name = '$table_name'
					;";
		return consultar_fuente($sql);
	}
}
?>