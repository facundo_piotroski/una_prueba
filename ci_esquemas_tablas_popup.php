<?php
require_once('dao_consulta_sql.php');
class ci_esquemas_tablas_popup extends toba_ci
{
	protected $s__datos;
	protected $s__datos_formulario;
	protected $s__datos_filtro;
	protected $s__where;
		
	function get_cn()
	{
		return $this->controlador->cn();
	}
	
	//---------------------------------------------------------------------------------
    //---- Eventos del CI -------------------------------------------------------------
	//---------------------------------------------------------------------------------	
	function evt__nuevo()
	{
		$this->set_pantalla('pant_edicion');		
	}
	function evt__volver()
	{
		$this->set_pantalla('pant_inicial');		
	}
	
	//-----------------------------------------------------------------------------------
	//---- cuadro -----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	function conf__cuadro_consultas(toba_ei_cuadro $cuadro)
	{
		$this->cn()->resetear();
		if (isset($this->s__datos_filtro)) {
			$datos = dao_consulta_sql::get_consultas($this->s__where);
			if($datos){
				$cuadro->set_datos($datos);
			}
		}else{
			$datos = dao_consulta_sql::get_consultas();
			if($datos){
				$cuadro->set_datos($datos);
			}
		}	 
	}
	
	function evt__cuadro_consultas__seleccion($seleccion)
	{	
		if ($this->cn()->hay_cursor_consulta_sql())
		{
				$this->cn()->resetear();
		}
		$this->cn()->cargar_consulta_sql($seleccion);
		$id_memoria = $this->cn()->traer_consulta_sql($seleccion);
		$this->cn()->set_cursor_consulta_sql($id_memoria[0]);	
		$this->set_pantalla('pant_edicion');		
	}
	
	function evt__cuadro_consultas__eliminar($seleccion)
	{
		$this->cn()->cargar_consulta_sql($seleccion);
		$id_memoria = $this->cn()->traer_id_memoria_consulta_sql($seleccion);
		$this->cn()->eliminar_consulta_sql($id_memoria[0]);
		try{
			$this->cn()->guardar();
		}catch (toba_error_db $error) {
			$codigo = $error->get_sqlstate();
			leu_excepcion::get_mensaje_error($codigo, 'Consulta', 'La ');
			$this->cn()->resetear();
		}		
	}
	
	//---------------------------------------------------------------------------------
    //---- Filtro ---------------------------------------------------------------------
	//--------------------------------------------------------------------------------- 		
	function conf__filtro_consultas_sql(toba_ei_filtro $filtro)
	{
		if (isset($this->s__datos_filtro)) {			
			$filtro->set_datos($this->s__datos_filtro);
			$this->s__where = $filtro->get_sql_where();
		}
	}
    
	function evt__filtro_consultas_sql__filtrar($datos)
	{
		$this->s__datos_filtro = $datos;
	}
    
	function evt__filtro_consultas_sql__cancelar()
	{
		unset($this->s__datos_filtro);
	}
	
	//-----------------------------------------------------------------------------------
	//---- form --------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------	
	function conf__form_formulario_consulta(toba_ei_formulario $form)
	{
		if($this->s__datos_formulario){ 
			$form->set_datos($this->s__datos_formulario);
			unset($this->s__datos_formulario);
		}
		//$tablas_esquemas = dao_consulta_sql::get_tablas_esquemas();
		//ei_arbol($tablas_esquemas);
		if ($this->cn()->hay_cursor_consulta_sql())
		{		
	        $datos = $this->get_cn()->get_consulta_sql();
			$form->set_datos($datos);
		}
	}
	
	function evt__form_formulario_consulta__modificacion($datos)
	{
		$this->cn()->set_consulta_sql($datos);
			try{
				$this->cn()->guardar();
				toba::notificacion()->agregar(' Los datos de la Consulta se guardaron correctamente.', 'info');
			}catch (toba_error_db $error) {
					$codigo = $error->get_sqlstate();
					leu_excepcion::get_mensaje_error($codigo, ' Consulta ', 'La ');
					$this->cn()->resetear();
			}
			$this->set_pantalla('pant_inicial');
	}
	
	function evt__form_formulario_consulta__alta($datos)
	{
		$this->cn()->agregar_consulta_sql($datos);
		toba::notificacion()->agregar('Los datos de la Consulta se guardaron correctamente.','info');
		$this->set_pantalla('pant_inicial');
	}
	
	function evt__form_formulario_consulta__ejecutar_SQL($datos)
	{
		$this->s__datos_formulario = $datos;
		
		$palabra = 'SIN_COINCIDENCIA';
		$keys[0]= 'DELETE';
		$keys[1]= 'UPDATE';
		$keys[2]= 'INSERT';
		$keys[3]= 'DROP';
		$keys[4]= 'CREATE';
		$keys[5]= 'REPLACE';
		foreach ($keys as $key){
			$regex = '/[^\s,.]*'.preg_quote($key).'[^\s,.]*/i';			
			if (preg_match_all($regex, $datos['consulta_sql'], $matches)) {
				toba::notificacion()->agregar('Solo se permiten consultas SELECT','error');
				$palabra = 'CON_COINCIDENCIA';
			}			
		}		
		if (!empty($datos['consulta_sql'])&& $palabra == 'SIN_COINCIDENCIA') {
			$datos = dao_consulta_sql::ejecutar_consulta($datos['consulta_sql']);
			$this->s__datos = $datos;
			$usuario = toba::usuario()->get_id();
			$datos['usuario_ultima_ejecucion'] = $usuario;
			$datos['fecha_ultima_ejecucion'] = date('Y-m-d H:i');
			if ($this->cn()->hay_cursor_consulta_sql())
			{	
				$this->cn()->set_consulta_sql($datos);
				$this->cn()->guardar();
			}
		}
	}
	
	//-----------------------------------------------------------------------------------
	//---- cuadro -----------------------------------------------------------------------
	//-----------------------------------------------------------------------------------
	function conf__cuadro_resultado_consulta(toba_ei_cuadro $cuadro)
	{
        if (!empty($this->s__datos)) {
            $datos = $this->s__datos;
            foreach ($datos[0] as $k => $v) {
                $columnas[] = array('clave' => $k, 'titulo' => $k);
            }
            $cuadro->limpiar_columnas();
            $cuadro->agregar_columnas($columnas);
            $cuadro->set_datos($this->s__datos);
			unset($this->s__datos);
        }   
	}
}
?>